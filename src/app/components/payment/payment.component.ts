import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})

export class PaymentComponent implements OnInit {

  selected: any;

  constructor() {
  }
  cities = [
    {name: 'Thanh toán thẻ ghi nợ nội địa (ATM)', code: 'ATM'},
    {name: 'Thanh tón QR code', code: 'QRC'},
  ];
  infoUser = [
    {
      mahs: 'A0111101010',
      hoten: 'Lê Văn Quyết',
      namhoc: '2020 - 2021',
      ky: 'Đợt 1',
      cap: 'Tiểu học - Khu đô thị Geleximco'
    }
  ];

  ngOnInit(): void {
  }

}
