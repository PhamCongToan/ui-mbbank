import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  items: MenuItem[] = [];
  userName = 'ToanPC';

  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Thông tin học phí',
        icon: 'pi pi-fw pi-home'
      },
      {
        label: 'Quản lý thông tin',
        icon: 'pi pi-fw pi-file'
      },
      {
        label: 'Báo cáo',
        icon: 'pi pi-fw pi-user'
      },
    ];
  }

}
