import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/layouts/footer/footer.component';
import { HeaderComponent } from './components/layouts/header/header.component';
import {MenubarModule} from 'primeng/menubar';
import { FormLoginComponent } from './components/auth/form-login/form-login.component';
import {InputTextModule} from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import {ButtonModule} from 'primeng/button';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PaymentComponent } from './components/payment/payment.component';
import {DropdownModule} from 'primeng/dropdown';
import {FormsModule} from "@angular/forms";
import { PaymentAtmComponent } from './components/payment-atm/payment-atm.component';
import { PaymentQrcodeComponent } from './components/payment-qrcode/payment-qrcode.component';
import {InputMaskModule} from 'primeng/inputmask';
import { QRCodeModule } from 'angularx-qrcode';
import { ForgotPassComponent } from './components/auth/forgot-pass/forgot-pass.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    FormLoginComponent,
    DashboardComponent,
    PaymentComponent,
    PaymentAtmComponent,
    PaymentQrcodeComponent,
    ForgotPassComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenubarModule,
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    BrowserAnimationsModule,
    DropdownModule,
    FormsModule,
    InputMaskModule,
    QRCodeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
